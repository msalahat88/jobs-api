class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.string :email
      t.string :username
      t.string :password
      t.string :gender
      t.integer :phone
      t.string :first_name
      t.string :last_name
      t.timestamps
    end
  end
end
