# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
require 'bcrypt'

@password = BCrypt::Password.create("mypassword")

arr = ["salahat", "mohammad", "admin", "user"]

# using for loop
arr.each { |i|

  @account = Account.where(username: i).first

  unless @account
    Account.create(username: i, password: @password)
  end

}

