# All phony commands are listed here
.PHONY: \
	all \
	env \
	docker \
	docker-build \
	docker-run \
	docker-install \
	migrate \
	seed

all: \
	env \
	docker

# Setup the environment file
env:
	cat .env.local >> .env

# Docker setup
docker: \
	docker-build \
	docker-run \
	migrate \
	seed

docker-build:
	docker-compose build

docker-run:
	docker-compose up -d

# Database seed
migrate:
	docker exec -it TASK_RUBY rails db:migrate

# Database migrate
seed:
	docker exec -it TASK_RUBY rails db:migrate
