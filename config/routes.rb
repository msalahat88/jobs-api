Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :v1 do

    #start admin panel routes
    namespace :admin do
      post "/login", to: "user#login"
      get "/my_profile", to:"user#my_profile"

      namespace :manage do

        namespace :jobs do
          post "/", to: "jobs#create"
          put "/", to: "jobs#update"
          delete "/{job-id}", to: "jobs#delete"
          get "/{job-id}", to: "jobs#view"
        end

        namespace :applicant do
          get "/applicants" , to: "job#applicants"
        end
      end


    end # admin panel routes

    # start candidate api
    namespace :candidate do

      post "/login", to: "user#login"
      get "/my_profile", to: "user#myProfile"
      namespace :jobs do

        get "/all", to: "jobs#all"
        get "/{job-uri}", to: "jobs#view"

      end
    end
  end
end
