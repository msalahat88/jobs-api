# Be sure to restart your server when you modify this file.

ActiveSupport::Reloader.to_prepare do
  AdminController.renderer.defaults.merge!(
    http_host: '',
    https: false
  )
  CandidateController.renderer.defaults.merge!(
    http_host: '',
    https: false
  )
end
