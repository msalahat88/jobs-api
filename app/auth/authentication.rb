require_relative '../models/account'
require 'logger'

class Authentication
  def initialize(username,password)
    @logger = Logger.new(STDOUT)
    @password = password
    @user = Account.where(username: username).first
  end

  def authenticate

    @user && @user.authenticate(@password)
  end

  def generate_token
    JsonWebToken.encode(user_id: @user.id)
  end

  def user_information
    @user
  end

end