require 'bcrypt'

class Account < ApplicationRecord

  def authenticate(password)

    BCrypt::Password.new(self.password) == password
  end
end
