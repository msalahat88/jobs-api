class V1::Candidate::CandidatesController < CandidateController
  def show
    @candidate = Candidate.all

    render json: @candidate, status: :ok
  end

  def create
    @candidate = Candidate.create(candidate_parameters)

    @candidate.save
    render json: @candidate, status: :created
  end

  private


  def candidate_parameters
    params.require(:candidate).permit(:email,:username,:gender,:phone,:first_name,:last_name)
  end
end
