class V1::Admin::UserController < AdminController

  ##
  # @author Mohammad M Salahat (msalahat88@gmail.com)
  # @note this api for login user using username and password
  # @api $base_url/v1/login
  # @version v1
  ##
  def login

    #
    # create new object for authentication class and pass to parameter username and pass
    #
    auth_object = Authentication.new(login_params[:username],login_params[:password])

    #
    # check from object auth user information valid or not
    #
    if auth_object.authenticate

      #
      # return json data to client side with status 200
      #
      render json: {
        message: "Login successful!",
        user: {
          id: auth_object.user_information.id,
          username: auth_object.user_information.username,
          token: auth_object.generate_token
        }
      },
             status: :ok

    else

      #
      # return json data to client side with status 401
      #
      render json: {
        message: "Incorrect username/password combination"
      },
             status: :unauthorized

    end #else

  end #login def


  ##
  # @author Mohammad M Salahat (msalahat88@gmail.com)
  # @note this api for get user information
  # @api $base_url/v1/my_profile
  # @version v1
  ##
  def my_profile

    render json: {  user_id: @current_user.id , username: @current_user.username }, status: :ok
  end

  private def login_params

    params.require(:user).permit(:username, :password)

  end #login_params def

end
