require_relative '../auth/authentication'

class CandidateController < ActionController::API

  include ExceptionHandler

  before_action :authorize_request, except: :login

  def authorize_request

    header = request.headers['Authorization']

    header = header.split(' ').last if header

    begin

      @decoded = JsonWebToken.decode(header)

      if @decoded

        @current_user = Candidate.find(@decoded[:user_id])

      else

        render json: { errors: 'unauthorized' }, status: :unauthorized

      end

    rescue ActiveRecord::RecordNotFound => e

      render json: { errors: e.message }, status: :unauthorized

    rescue JWT::DecodeError => e

      render json: { errors: e.message }, status: :unauthorized

    end

  end

end
